<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::controller(\App\Http\Controllers\User\AuthController::class)->group(function () {
    Route::post('login', 'login')->name('login');
    Route::post('register', 'register');
    Route::post('logout', 'logout');
    Route::post('refresh', 'refresh');
});

Route::middleware('api')->group(function () {
    Route::controller(\App\Http\Controllers\Vacancy\VacancyController::class)->group(function () {
        Route::get('/vacancy/list', 'list');
        Route::post('/vacancy/create', 'create');
        Route::post('/vacancy/{vacancy}/response', 'sendResponse');
        Route::get('/vacancy/{vacancy}/response', 'responses');
        Route::get('/vacancy/{vacancy}', 'view');
    });
});


