<?php

return [
    'max_balance' => 5,
    'day_up' => 1,
    'max_vacancy_per_day' => 2,

    \App\Enums\CoinActionEnum::CREATE_VACANCY->value => 2,
    \App\Enums\CoinActionEnum::CREATE_RESPONSE->value => 1,
];
