<?php

namespace App\Services\Vacancy;

use App\Models\User;
use App\Models\Vacancy;
use App\Enums\CoinActionEnum;
use App\Services\User\CoinService;
use Illuminate\Support\Facades\Config;

class CreateService
{
    private CoinService $coinService;

    public function __construct(CoinService $coinService)
    {
        $this->coinService = $coinService;
    }

    public function create(string $title, string $description, User $user): Vacancy
    {
        $vacancy = Vacancy::create($user, $title, $description);
        $config = Config::get('coins');

        // Limit on day
        $vacancies = Vacancy::getCountPublishByDay(new \DateTime(), $user);
        if($vacancies >= $config['max_vacancy_per_day']) {
            throw new \Exception('You use limit for vacancies on day');
        }

        // Payment
        $coins = $this->coinService->assessment(CoinActionEnum::CREATE_VACANCY, $user);
        if(!$coins) {
            throw new \Exception('Need more coins on balance');
        }

        $this->coinService->charge(CoinActionEnum::CREATE_VACANCY, $user);
        $vacancy->save();

        return $vacancy;
    }
}
