<?php

namespace App\Services\Vacancy;

use App\Models\User;
use App\Models\Vacancy;
use App\Enums\CoinActionEnum;
use App\Models\VacancyResponse;
use App\Services\User\CoinService;
use Illuminate\Support\Facades\Config;

class AddResponseService
{
    private CoinService $coinService;

    public function __construct(CoinService $coinService)
    {
        $this->coinService = $coinService;
    }

    public function add(Vacancy $vacancy, User $user, string $text): VacancyResponse
    {
        if ($vacancy->user()->first()->id === $user->id) {
            throw new \Exception('You cannot apply for your own job posting');
        }

        if (VacancyResponse::findUserResponse($vacancy, $user) > 0) {
            throw new \Exception('You have already responded to this vacancy');
        }

        $coins = $this->coinService->assessment(CoinActionEnum::CREATE_RESPONSE, $user);
        if(!$coins) {
            throw new \Exception('Need more coins on balance');
        }

        $response = VacancyResponse::create($user, $vacancy, $text);
        $this->coinService->charge(CoinActionEnum::CREATE_RESPONSE, $user);

        $response->save();

        return  $response;
    }
}
