<?php

namespace App\Services\User;

use App\Models\User;
use App\Enums\CoinActionEnum;
use Illuminate\Support\Facades\Config;

class CoinService
{
    public function assessment(CoinActionEnum $action, User $user): bool
    {
        $price = $this->getActionPrice($action);
        if($price === null) {
            return false;
        }

        return $price <= $user->coins;
    }

    public function charge(CoinActionEnum $action, User $user): void
    {
        if(!$this->assessment($action, $user)) {
            throw new \Exception('Need more coins on balance');
        }

        $price = $this->getActionPrice($action);

        $user->coins = $user->coins - $price;
        $user->save();
    }

    private function getActionPrice(CoinActionEnum $action): ?int
    {
        $config = Config::get('coins');
        if(!isset($config[$action->value])) {
            return null;
        }

        return $config[$action->value];
    }
}
