<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class VacancyResponse extends Model
{
    use Notifiable;

    protected $table = 'vacancies_response';

    protected $fillable = [
        'text',
        'user_id',
        'vacancy_id',
    ];

    public static function getTableName(): string
    {
        $model = new VacancyResponse();

        return $model->table;
    }

    public static function create(User $user, Vacancy $vacancy, string $text): self
    {
        $response = new VacancyResponse([
            'user_id' => $user->id,
            'text' => $text,
            'vacancy_id' => $vacancy->id,
        ]);

        return $response;
    }

    public static function findUserResponse(Vacancy $vacancy, User $user)
    {
        $query = self::query()
            ->where("vacancy_id", '=', $vacancy->id)
            ->where("user_id", '=', $user->id);

        return $query->count();
    }
}
