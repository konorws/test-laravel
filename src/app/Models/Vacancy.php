<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Vacancy extends Model
{
    use Notifiable;

    protected $table = 'vacancies';

    protected $fillable = [
        'title',
        'description',
        'user_id'
    ];

    public static function create(User $user, string $title, string $description): self
    {
        $vacancy = new Vacancy([
            'user_id' => $user->id,
            'title' => $title,
            'description' => $description,
        ]);

        return $vacancy;
    }

    public function responses()
    {
        return $this->hasMany(VacancyResponse::class, 'vacancy_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function getCountPublishByDay(\DateTimeInterface $dateTime, User $user)
    {
        $results = self::query()
            ->where("user_id", '=', $user->id)
            ->whereBetween("created_at", [
                new \DateTime($dateTime->format("Y-m-d"). " 00:00:00"),
                $dateTime
            ]);

        return $results->count();
    }
}
