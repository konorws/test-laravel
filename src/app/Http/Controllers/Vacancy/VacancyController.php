<?php

namespace App\Http\Controllers\Vacancy;

use App\Http\Resources\VacancyResponseResource;
use App\Models\Vacancy;
use App\Services\Vacancy\AddResponseService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PHPOpenSourceSaver\JWTAuth\JWTAuth;
use App\Http\Resources\VacancyResource;
use App\Services\Vacancy\CreateService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class VacancyController extends Controller
{

    private Request $request;
    private JWTAuth $auth;

    public function __construct(Request $request, JWTAuth $auth)
    {
        $this->middleware('api');
        $this->request = $request;
        $this->auth = $auth;
    }

    public function create(CreateService $createService): VacancyResource
    {
        $this->request->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string',
        ]);

        $user = $this->auth->user();

        $vacancy = $createService->create(
            $this->request->input('title'),
            $this->request->input('description'),
            $user
        );

        return new VacancyResource($vacancy);
    }

    public function list(): AnonymousResourceCollection
    {
        $vacancies = Vacancy::all();

        return VacancyResource::collection($vacancies);
    }

    public function view(Vacancy $vacancy): VacancyResource
    {
        return new VacancyResource($vacancy);
    }

    public function sendResponse(Vacancy $vacancy, AddResponseService $addResponseService)
    {
        $this->request->validate([
            'text' => 'required|string|min:10',
        ]);

        $user = $this->auth->user();

        $response = $addResponseService->add($vacancy, $user, $this->request->input('text'));

        return new VacancyResponseResource($response);
    }

    public function responses(Vacancy $vacancy): AnonymousResourceCollection
    {
        return VacancyResponseResource::collection($vacancy->responses()->get());
    }
}
