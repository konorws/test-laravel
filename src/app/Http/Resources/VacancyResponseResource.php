<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VacancyResponseResource extends JsonResource
{

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'vacancy_id' => $this->vacancy_id,
            'text' => $this->text,
            'created_at' => $this->created_at
        ];
    }
}
