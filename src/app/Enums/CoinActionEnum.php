<?php

namespace App\Enums;

enum CoinActionEnum: string
{
    case CREATE_VACANCY = 'price_vacancy';
    case CREATE_RESPONSE = 'price_response';
}
